//
//  ViewController.swift
//  Concentration
//
//  Created by Sebastian Stefaniak on 10.07.2018.
//  Copyright © 2018 StefApps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private lazy var game = Concentration(numberOfPairsOfCards: (numberOfPairdOfCards))
    private lazy var emojiChoices: String = getRandomTheme()
    var numberOfPairdOfCards: Int{
        return (cardButtons.count + 1 ) / 2
    }
    // MARK: TASKS 5 & 6
    private var emojiThemes: [String: String] = ["halloween": "👻🎃💀👾🤖🤡", "faces": "😀😁😎😇😡😤", "sport": "⚽️⛸🏀🏈🎽🎾", "cars": "🚗🚕🚙🚌🚎🏎", "items": "⌚️📱💻⌨️💿🕹", "animals": "🐶🐱🐭🐹🐰🦊"]
    private var emoji = [Card: String]()
    
    @IBOutlet private weak var newGameButton: UIButton!
    @IBOutlet private weak var flipCountLabel: UILabel!
    @IBOutlet private var cardButtons: [UIButton]!
    @IBOutlet private weak var gameScore: UILabel!
    
    @IBAction private func touchCard(_ sender: UIButton) {
        if let cardNumber = cardButtons.index(of: sender){
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
    }
    
    @IBAction private func startNewGame(_ sender: UIButton) {
        startNewGame()
    }
    
    private func startNewGame(){
        emojiChoices = getRandomTheme()
        emoji.removeAll()
        game.startNewGame()
        updateViewFromModel()
    }
    
    // MARK: TASK 5
    private func getRandomTheme() -> String{
        return Array(emojiThemes.values)[emojiThemes.count.arc4random]
    }
    
    private func updateViewFromModel(){
        flipCountLabel.text = "Flips: \(game.flipCount)"
        gameScore.text = "Score: \(game.score)"
        for index in cardButtons.indices{
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp{
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = .white
                button.isUserInteractionEnabled = false
            }else{
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                button.isUserInteractionEnabled = card.isMatched ? false : true
            }
        }
    }
    
    private func emoji(for card: Card)->String{
        if emoji[card] == nil, emojiChoices.count > 0{
            let randomStringIndex = emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.arc4random)
            emoji[card] = String(emojiChoices.remove(at: randomStringIndex))
        }
        return emoji[card] ?? "?"
    }
}
