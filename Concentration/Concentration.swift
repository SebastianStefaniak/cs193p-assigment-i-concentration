//
//  Concentration.swift
//  Concentration
//
//  Created by Sebastian Stefaniak on 10.07.2018.
//  Copyright © 2018 StefApps. All rights reserved.
//

import Foundation

struct Concentration{
    // MARK: TASKS 7 & 8
    var flipCount = 0, score = 0
    private(set) var cards: [Card] = []
    private var indexOfOneAndOnlyFaceUpCard: Int?{
        get{
            return cards.indices.filter{cards[$0].isFaceUp}.oneAndOnly
        }
        set{
            for index in cards.indices{
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    mutating func chooseCard(at index: Int){
        assert(cards.indices.contains(index), "Concentration.choseCard(at: \(index): chosen index not in the cards)")
        if !cards[index].isMatched{
            flipCount += 1
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index{
                if cards[matchIndex] == cards[index]{
                    cards[index].isMatched = true
                    cards[matchIndex].isMatched = true
                    score += 2
                }else {
                    checkMismatches(firstCardIndex: index, matchCardIndex: matchIndex)
                }
                cards[index].isFaceUp = true
            }else {
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    // MARK: TASK 7
    mutating func checkMismatches(firstCardIndex index: Int, matchCardIndex matchIndex: Int){
        if cards[index].wasFlipped{
            score -= 1
        }
        if cards.filter({$0 == cards[matchIndex] && $0.wasFlipped == true}).first != nil{
            score -= 1
        }
        cards[index].wasFlipped = true
        cards[matchIndex].wasFlipped = true
    }
    
    // MARK: TASK 3
    mutating func startNewGame(){
        flipCount = 0
        score = 0
        for cardIndex in cards.indices{
            cards[cardIndex].isFaceUp = false
            cards[cardIndex].isMatched = false
            cards[cardIndex].wasFlipped = false
            indexOfOneAndOnlyFaceUpCard = nil
        }
        shuffleCards()
    }
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "Concentration.init(numberOfParisOfCards \(numberOfPairsOfCards): yout must at least one pair of cards.")
        for _ in 0..<numberOfPairsOfCards{
            let card = Card()
            cards += [card, card]
        }
        shuffleCards()
    }
    
    // MARK: TASK 4
    mutating func shuffleCards(){
        var shuffledCards = [Card]()
        for _ in cards.indices{
            shuffledCards.append(cards.remove(at: cards.count.arc4random))
        }
        cards += shuffledCards
    }
}

extension Collection{
    var oneAndOnly: Element?{
        return count == 1 ? first : nil
    }
}

extension Int{
    var arc4random: Int{
        if self > 0{
            return Int(arc4random_uniform(UInt32(self)))
        }else if self < 0{
            return Int(arc4random_uniform(UInt32(self)))
        }else{
            return 0
        }
    }
}
