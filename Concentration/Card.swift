//
//  Card.swift
//  Concentration
//
//  Created by Sebastian Stefaniak on 10.07.2018.
//  Copyright © 2018 StefApps. All rights reserved.
//

import Foundation

struct Card: Hashable{
    var isFaceUp = false, isMatched = false, wasFlipped = false
    private var identifier: Int
    var hashValue: Int{
        return identifier
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool{
        return lhs.identifier == rhs.identifier
    }
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int{
        identifierFactory += 1
        return identifierFactory
    }
    
    init(){
        self.identifier = Card.getUniqueIdentifier()
    }
}
